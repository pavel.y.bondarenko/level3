select c.name        as city,
       str_type.name as s_type,
       str.name      as street,
       a.number      as number,
       s.name        as store_name,
       sum           as amount
from (select sia.store_id as store_id, sum(sia.amount) as sum
      from (select store_id, amount
            from product_in_store
            where product_id in (select id
                                 from product
                                 where category_id = (select id
                                                      from product_category
                                                      where name = ?))) as sia
      group by sia.store_id
      order by sum desc
      limit 1) as sum_table
         join store as s on s.id = sum_table.store_id
         join address as a on a.id = s.address_id
         join street as str on str.id = a.street_id
         join street_type as str_type on str.type_id = str_type.id
         join city as c on str.city_id = c.id