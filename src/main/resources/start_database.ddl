drop table if exists product_in_store, store, address, street, street_type, city, product, product_category;

create table city
(
    id   serial primary key,
    name varchar(255)
);

create table street_type
(
    id   serial primary key,
    name varchar(255)
);

create table street
(
    id      serial primary key,
    name    varchar(255),
    type_id int references street_type (id) on delete cascade,
    city_id int references city (id) on delete cascade
);

create table address
(
    id        serial primary key,
    number    int,
    street_id int references street (id) on delete cascade
);

create table store
(
    id         serial primary key,
    name       varchar(255),
    address_id int references address (id) on delete cascade
);

create table product_category
(
    id   serial primary key,
    name varchar(255)
);

create table product
(
    id          serial primary key,
    name        varchar(255),
    category_id int references product_category (id) on delete cascade
);

create table product_in_store
(
    id         serial primary key,
    store_id   int references store (id) on delete cascade,
    product_id int references product (id) on delete cascade,
    amount     int,
    lot_number int
);