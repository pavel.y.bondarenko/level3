create index on product_in_store (product_id);
create index on product_in_store (store_id);
create index on product_category (name);
create index on product (category_id);
create index on address (street_id);
create index on store (address_id);
create index on street (city_id);
create index on street (type_id);
-- create index on city (name);
-- create index on street_type (name);
-- create index on street (name);
-- create index on address (number);
-- create index on store (name);

