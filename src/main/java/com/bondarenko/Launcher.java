package com.bondarenko;

import com.bondarenko.exception.PropertyFileDoesNotExistsException;
import com.bondarenko.exception.PropertyMissedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.LinkedBlockingQueue;

public class Launcher {
    private static final Logger LOGGER = LoggerFactory.getLogger(Launcher.class);
    private static final String PROPERTY_FILE_NAME = "app.properties";


    public static void main(String[] args) {
        PropertyService propertyService;
        try {
            propertyService = new PropertyService(PROPERTY_FILE_NAME);
        } catch (PropertyFileDoesNotExistsException | PropertyMissedException e) {
            LOGGER.error("Some problems with properties", e);
            return;
        }

        new App(propertyService,
                new FileService(),
                new ExecutorCreator(),
                new LinkedBlockingQueue<>(),
                new DaoFactory())
                .run();

        LOGGER.info("End of program!");
    }
}
