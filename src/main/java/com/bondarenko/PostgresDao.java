package com.bondarenko;

import com.bondarenko.exception.PostgresConnectionException;
import com.bondarenko.pojo.StoreWithProductCategory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.stream.IntStream;

public class PostgresDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(PostgresDao.class);
    private static final String DDL_SQL_FILE_NAME = "start_database.ddl";
    private static final String DML_SQL_FILE_NAME = "data.sql";
    private static final String INDEXES_SQL_FILE_NAME = "indexes.sql";
    private static final String FIND_STORE_WITH_MAX_PRODUCTS_BY_CATEGORY_SQL_FILE_NAME = "find_store_with_max_products_by_category.sql";
    private static final String CATEGORY_DEFAULT_PREFIX = "category ";
    private static final String PRODUCT_DEFAULT_PREFIX = "product ";
    private static final String STORE_MAX_ID_SQL = "select max(id) from store";
    private static final String PRODUCT_MAX_ID_SQL = "select max(id) from product";
    private static final String CATEGORY_MAX_ID_SQL = "select max(id) from product_category";
    private static final String ADD_PRODUCT_CATEGORY_TEMPLATE = "insert into product_category (name) values (?)";
    private static final String ADD_PRODUCT_TEMPLATE = "insert into product (name, category_id) values (?, ?)";
    private static final String ADD_PRODUCT_IN_STORE_SQL_TEMPLATE = "insert into product_in_store "
                                                                  + "(store_id, product_id, amount, lot_number) "
                                                                  + "values (?, ?, ?, ?)";
    private final Connection connection;
    private final FileService fileService;
    private final String findStoreWithMaxProductByCategorySql;

    public PostgresDao(Connection connection,
                       FileService fileService) {
        this.connection = connection;
        this.fileService = fileService;
        findStoreWithMaxProductByCategorySql = fileService.readAllFile(FIND_STORE_WITH_MAX_PRODUCTS_BY_CATEGORY_SQL_FILE_NAME);
    }

    public int getStoreMaxId() {
        try (Statement statement = connection.createStatement()) {
            ResultSet storeMaxIdResultSet = statement.executeQuery(STORE_MAX_ID_SQL);
            if (storeMaxIdResultSet.next()) {
                int storeMaxId = storeMaxIdResultSet.getInt(1);
                if (storeMaxId > 0) {
                    LOGGER.debug("Store max id = {}", storeMaxId);
                    return storeMaxId;
                } else {
                    LOGGER.error("There is no rows in 'store' table");
                    throw new PostgresConnectionException("There is no rows in 'store' table");
                }
            } else {
                throw new PostgresConnectionException("Something wrong with table 'store'");
            }
        } catch (SQLException e) {
            throw new PostgresConnectionException(e);
        }
    }

    public int getProductMaxId() {
        try (Statement statement = connection.createStatement()) {
            ResultSet productMaxIdResultSet = statement.executeQuery(PRODUCT_MAX_ID_SQL);
            if (productMaxIdResultSet.next()) {
                int productMaxId = productMaxIdResultSet.getInt(1);
                if (productMaxId > 0) {
                    LOGGER.debug("Product max id = {}", productMaxId);
                    return productMaxId;
                } else {
                    LOGGER.error("There is no rows in 'product' table");
                    throw new PostgresConnectionException("There is no rows in 'product' table");
                }
            } else {
                throw new PostgresConnectionException("Something wrong with table 'product'");
            }
        } catch (SQLException e) {
            throw new PostgresConnectionException(e);
        }
    }

    public int getCategoryMaxId() {
        try (Statement statement = connection.createStatement()) {
            ResultSet categoryMaxIdResultSet = statement.executeQuery(CATEGORY_MAX_ID_SQL);
            if (categoryMaxIdResultSet.next()) {
                int categoryMaxId = categoryMaxIdResultSet.getInt(1);
                if (categoryMaxId > 0) {
                    LOGGER.debug("Category max id = {}", categoryMaxId);
                    return categoryMaxId;
                } else {
                    LOGGER.error("There is no rows in 'product_category' table");
                    throw new PostgresConnectionException("There is no rows in 'product_category' table");
                }
            } else {
                throw new PostgresConnectionException("Something wrong with table 'product_category'");
            }
        } catch (SQLException e) {
            throw new PostgresConnectionException(e);
        }
    }

    public void addProductCategories(int numberOfCategories,
                                     int batchMaxSize) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(ADD_PRODUCT_CATEGORY_TEMPLATE)) {
            IntStream.range(1, numberOfCategories + 1).forEach(categoryNumber -> {
                try {
                    preparedStatement.setString(1, CATEGORY_DEFAULT_PREFIX + categoryNumber);
                    preparedStatement.addBatch();
                    if ((categoryNumber % batchMaxSize == 0) || (categoryNumber == numberOfCategories)) {
                        preparedStatement.executeBatch();
                    }
                } catch (SQLException e) {
                    throw new PostgresConnectionException("Can not fill product_category table", e);
                }
            });
        } catch (SQLException e) {
            throw new PostgresConnectionException("Can not create prepared statement for add product categories", e);
        }

        LOGGER.debug("Product categories added successfully!");
    }

    public void addProducts(int categoryMaxId,
                            int numberOfProductsPerCategory) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(ADD_PRODUCT_TEMPLATE)) {
            IntStream.range(1, categoryMaxId + 1).forEach(categoryId -> {
                int startProductNumber = (categoryId - 1) * numberOfProductsPerCategory + 1;
                int endProductNumberExclusive = categoryId * numberOfProductsPerCategory + 1;
                IntStream.range(startProductNumber, endProductNumberExclusive).forEach(productNumber -> {
                try {
                    preparedStatement.setString(1, PRODUCT_DEFAULT_PREFIX + productNumber);
                    preparedStatement.setInt(2, categoryId);
                    preparedStatement.addBatch();
                } catch (SQLException e) {
                    throw new PostgresConnectionException("Can not fill prepared statement", e);
                }
                });
                try {
                    preparedStatement.executeBatch();
                } catch (SQLException e) {
                    throw new PostgresConnectionException("Can not fill execute batch", e);
                }
            });
        } catch (SQLException e) {
            throw new PostgresConnectionException("Can not create prepared statement for add product categories", e);
        }

        LOGGER.debug("Products added successfully!");
    }

    private void execute(String sql) {
        try (Statement statement = connection.createStatement()) {
            statement.execute(sql);
            LOGGER.info("Sql [{}] executed!", sql);
        } catch (SQLException e) {
            throw new PostgresConnectionException("Can not execute - " + sql, e);
        }
    }

    public PreparedStatement getPreparedStatementForAddProductInStore() {
        try {
            return connection.prepareStatement(ADD_PRODUCT_IN_STORE_SQL_TEMPLATE);
        } catch (SQLException e) {
            throw new PostgresConnectionException("Can not create prepared statement", e);
        }
    }

    public void createDatabase() {
        execute(fileService.readAllFile(DDL_SQL_FILE_NAME));
        LOGGER.debug("Database was successfully created!");
    }

    public void fillDatabase() {
        execute(fileService.readAllFile(DML_SQL_FILE_NAME));
        LOGGER.debug("Database was successfully filled with start data!");
    }

    public void createIndexes() {
        execute(fileService.readAllFile(INDEXES_SQL_FILE_NAME));
        LOGGER.debug("Indexes was successfully created!");
    }

    public StoreWithProductCategory getStoreWithMaxNumberOfProductsByCategory(String category) {
        StoreWithProductCategory storeWithProductCategory = new StoreWithProductCategory().setCategoryName(category);

        try (PreparedStatement preparedStatement = connection.prepareStatement(findStoreWithMaxProductByCategorySql)) {
            preparedStatement.setString(1, category);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                storeWithProductCategory.setCityName(resultSet.getString("city"));
                storeWithProductCategory.setStreetType(resultSet.getString("s_type"));
                storeWithProductCategory.setStreetName(resultSet.getString("street"));
                storeWithProductCategory.setHouseNumber(resultSet.getInt("number"));
                storeWithProductCategory.setStoreName(resultSet.getString("store_name"));
                storeWithProductCategory.setAmountOfCategory(resultSet.getInt("amount"));
            }
        } catch (SQLException e) {
            throw new PostgresConnectionException(e);
        }

        return storeWithProductCategory;
    }
}
