package com.bondarenko;

import com.bondarenko.exception.FileAccessException;
import com.bondarenko.exception.PostgresConnectionException;
import com.bondarenko.pojo.ProductInStore;
import com.bondarenko.pojo.StoreWithProductCategory;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

@RequiredArgsConstructor
public class App {
    private static final Logger LOGGER = LoggerFactory.getLogger(App.class);

    private static final int NUMBER_OF_CATEGORIES = 1_000;
    private static final int NUMBER_OF_PRODUCTS_PER_CATEGORY = 100;
    private static final int NUMBER_OF_GENERATORS = 1;
    private static final int NUMBER_OF_DB_WRITERS = 3;
    private static final int NUMBER_OF_RAWS = 3_000_000;
    private static final int BATCH_MAX_SIZE = 3_000;
    private static final int EXECUTOR_WAITING_TIME_IN_SECONDS = 600;
    private static final String STORE_WITH_MAX_PRODUCTS_BY_CATEGORY_OUT_TEMPLATE = "Товару категорії '%s' більше всього "
                                                                                 + "(%d шт.) у магазині '%s' за адресою: "
                                                                                 + "місто %s, %s %s, будинок %d";

    private final PropertyService propertyService;
    private final FileService fileService;
    private final ExecutorCreator executorCreator;
    private final BlockingQueue<ProductInStore> productInStoreQueue;
    private final DaoFactory daoFactory;

    public void run() {
        try (Connection connection = DriverManager.getConnection(propertyService.getDbUrl(),
                                                                 propertyService.getDbUsername(),
                                                                 propertyService.getDbPassword())) {
            PostgresDao postgresDao = daoFactory.getPostgresDao(connection, fileService);
            postgresDao.createDatabase();
            postgresDao.fillDatabase();
            postgresDao.addProductCategories(NUMBER_OF_CATEGORIES, BATCH_MAX_SIZE);
            postgresDao.addProducts(postgresDao.getCategoryMaxId(), NUMBER_OF_PRODUCTS_PER_CATEGORY);

            int storeMaxId = postgresDao.getStoreMaxId();
            int productMaxId = postgresDao.getProductMaxId();

            ProductInStoreGenerator generator = new ProductInStoreGenerator(storeMaxId, productMaxId, new Random());
            ProductInStoreValidator validator = new ProductInStoreValidator();

            StopWatch watch = new StopWatch();
            watch.start();

            ExecutorService generatorValidatorsExecutor = executorCreator.createGeneratorValidatorExecutor(NUMBER_OF_GENERATORS,
                                                                                                           NUMBER_OF_RAWS,
                                                                                                           productInStoreQueue,
                                                                                                           generator,
                                                                                                           validator);
            ExecutorService batchSendersExecutor = executorCreator.createBatchSenderExecutor(NUMBER_OF_DB_WRITERS,
                                                                                             productInStoreQueue,
                                                                                             fileService,
                                                                                             propertyService.getDbUrl(),
                                                                                             propertyService.getDbUsername(),
                                                                                             propertyService.getDbPassword(),
                                                                                             BATCH_MAX_SIZE);

            waitExecutor(generatorValidatorsExecutor);

            Stream.generate(ProductInStore::new)
                  .limit(NUMBER_OF_DB_WRITERS)
                  .forEach(productInStoreQueue::add);

            waitExecutor(batchSendersExecutor);

            watch.stop();
            LOGGER.info("Time of saving {} raws in DB = {} ms", NUMBER_OF_RAWS, watch.getTime());

            watch.reset();
            watch.start();
            LOGGER.info(createOutMessage(postgresDao.getStoreWithMaxNumberOfProductsByCategory(propertyService.getCategory())));
            watch.stop();
            LOGGER.info("Find store WITHOUT indexes for {} ms", watch.getTime());

            watch.reset();
            watch.start();
            postgresDao.createIndexes();
            watch.stop();
            LOGGER.info("Indexing for {} ms", watch.getTime());

            watch.reset();
            watch.start();
            LOGGER.info(createOutMessage(postgresDao.getStoreWithMaxNumberOfProductsByCategory(propertyService.getCategory())));
            watch.stop();
            LOGGER.info("Find store WITH indexes for {} ms", watch.getTime());

        } catch (PostgresConnectionException e) {
            LOGGER.error("Connection failed", e);
        } catch (FileAccessException e) {
            LOGGER.error("There are some troubles with file access", e);
        } catch (SQLException e) {
            LOGGER.error("Can not create connection", e);
        }
    }

    private void waitExecutor(ExecutorService executorService) {
        executorService.shutdown();
        try {
            if (!executorService.awaitTermination(EXECUTOR_WAITING_TIME_IN_SECONDS, TimeUnit.SECONDS)) {
                executorService.shutdownNow();
            }
        } catch (InterruptedException e) {
            LOGGER.warn("There is exception with executor shutdown", e);
            executorService.shutdownNow();
        }
    }

    private String createOutMessage(StoreWithProductCategory storeWithProductCategory) {
        return String.format(STORE_WITH_MAX_PRODUCTS_BY_CATEGORY_OUT_TEMPLATE,
                             storeWithProductCategory.getCategoryName(),
                             storeWithProductCategory.getAmountOfCategory(),
                             storeWithProductCategory.getStoreName(),
                             storeWithProductCategory.getCityName(),
                             storeWithProductCategory.getStreetType(),
                             storeWithProductCategory.getStreetName(),
                             storeWithProductCategory.getHouseNumber());
    }
}
