package com.bondarenko;

import com.bondarenko.exception.PostgresConnectionException;
import com.bondarenko.pojo.ProductInStore;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.concurrent.BlockingQueue;

@RequiredArgsConstructor
public class ProductInStoreBatchSender implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProductInStoreBatchSender.class);


    private final BlockingQueue<ProductInStore> productInStoreQueue;
    private final FileService fileService;
    private final String dbUrl;
    private final String dbUsername;
    private final String dbPassword;
    private final int batchMaxSize;

    @Override
    public void run() {
        LOGGER.debug("Batch sender start working!");
        try (Connection connection = DriverManager.getConnection(dbUrl, dbUsername, dbPassword)) {
            sendBatches(new PostgresDao(connection, fileService));
        } catch (SQLException e) {
            throw new PostgresConnectionException("Can not create connection", e);
        }
        LOGGER.debug("Sender finish!");
    }

    private void sendBatches(PostgresDao postgresDao) {
        boolean hasNoPoisonObject = true;
        try (PreparedStatement preparedStatement = postgresDao.getPreparedStatementForAddProductInStore()) {
            while (hasNoPoisonObject) {
                int count = 0;
                while (count < batchMaxSize && hasNoPoisonObject) {
                    ProductInStore productInStore = productInStoreQueue.poll();
                    if (productInStore != null) {
                        if (productInStore.getStoreId() > 0) {
                            preparedStatement.setInt(1, productInStore.getStoreId());
                            preparedStatement.setInt(2, productInStore.getProductId());
                            preparedStatement.setInt(3, productInStore.getAmount());
                            preparedStatement.setInt(4, productInStore.getLotNumber());
                            preparedStatement.addBatch();
                            count++;
                        } else {
                            hasNoPoisonObject = false;
                            LOGGER.debug("Poison object obtained!");
                        }
                    }
                }

                if (count > 0) {
                    preparedStatement.executeBatch();
                    LOGGER.debug("Batch with {} messages sent", count);
                }
                System.gc();
            }
        } catch (SQLException e) {
            throw new PostgresConnectionException("Can not send batch", e);
        }
    }
}
