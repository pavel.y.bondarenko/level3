package com.bondarenko;

import com.bondarenko.exception.FileAccessException;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

public class FileService {
    private static final Charset ENCODING = StandardCharsets.UTF_8;

    public String readAllFile(String fileName) {
        try {
            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
            InputStream resourceAsStream = classLoader.getResourceAsStream(fileName);
            return new BufferedReader(new InputStreamReader(resourceAsStream, ENCODING))
                    .lines()
                    .collect(Collectors.joining("\n"));
        } catch (Exception e) {
            throw new FileAccessException("Can not read file", e);
        }
    }
}