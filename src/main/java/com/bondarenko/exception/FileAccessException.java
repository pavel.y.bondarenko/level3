package com.bondarenko.exception;

public class FileAccessException extends RuntimeException {

    public FileAccessException(String message) {
        super(message);
    }

    public FileAccessException(String message, Throwable exception) {
        super(message, exception);
    }
}
