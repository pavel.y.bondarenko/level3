package com.bondarenko.exception;

public class PropertyMissedException extends RuntimeException {

    public PropertyMissedException(String message) {
        super(message);
    }
}
