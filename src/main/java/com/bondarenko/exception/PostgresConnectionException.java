package com.bondarenko.exception;

public class PostgresConnectionException extends RuntimeException {

    public PostgresConnectionException(Throwable cause) {
        super(cause);
    }

    public PostgresConnectionException(String message) {
        super(message);
    }

    public PostgresConnectionException(String message, Throwable cause) {
        super(message, cause);
    }
}
