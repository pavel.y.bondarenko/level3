package com.bondarenko.exception;

public class PropertyFileDoesNotExistsException extends RuntimeException {

    public PropertyFileDoesNotExistsException (Throwable throwable) {
        super(throwable);
    }
}
