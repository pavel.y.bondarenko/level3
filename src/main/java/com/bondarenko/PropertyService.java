package com.bondarenko;

import com.bondarenko.exception.PropertyFileDoesNotExistsException;
import com.bondarenko.exception.PropertyMissedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

public class PropertyService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PropertyService.class);
    private static final Charset PROPERTY_ENCODING = StandardCharsets.UTF_8;
    private static final String URL_PROPERTY_NAME = "dbUrl";
    private static final String USERNAME_PROPERTY_NAME = "dbUsername";
    private static final String PASSWORD_PROPERTY_NAME = "dbPassword";
    private static final String CATEGORY_PROPERTY_NAME = "category";
    private static final String PROPERTY_IS_MISSED_TEMPLATE = "Property '{}' is missed!";

    private final Properties properties;

    public PropertyService(String fileName) {
        properties = getProperties(fileName);
        if (isSomePropertyMissed()) {
            throw new PropertyMissedException("Some property is missed!");
        }
    }

    public String getDbUrl() {
        return properties.getProperty(URL_PROPERTY_NAME);
    }

    public String getDbUsername() {
        return properties.getProperty(USERNAME_PROPERTY_NAME);
    }

    public String getDbPassword() {
        return properties.getProperty(PASSWORD_PROPERTY_NAME);
    }

    public String getCategory() {
        return System.getProperty(CATEGORY_PROPERTY_NAME);
    }

    private Properties getProperties(String fileName) throws PropertyFileDoesNotExistsException {
        Properties properties = new Properties();

        File jarFile = new File(PropertyService.class.getProtectionDomain().getCodeSource().getLocation().getPath());
        File propertyFile = new File(jarFile.getParent(), fileName);

        try {
            if (propertyFile.exists()) {
                try (FileReader propertyFileReader = new FileReader(propertyFile, PROPERTY_ENCODING)) {
                    properties.load(propertyFileReader);
                }
                LOGGER.debug("External property file loaded - {}", propertyFile.getAbsolutePath());
            } else {
                ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
                InputStream resourceAsStream = classLoader.getResourceAsStream(fileName);
                properties.load(new InputStreamReader(resourceAsStream, PROPERTY_ENCODING));
                LOGGER.debug("Internal property file loaded");
            }
        } catch (NullPointerException | IOException e) {
            throw new PropertyFileDoesNotExistsException(e);
        }
        return properties;
    }

    private boolean isSomePropertyMissed() {
        boolean isMissed = false;

        if (!properties.containsKey(URL_PROPERTY_NAME)) {
            LOGGER.error(PROPERTY_IS_MISSED_TEMPLATE, URL_PROPERTY_NAME);
            isMissed = true;
        }

        if (!properties.containsKey(USERNAME_PROPERTY_NAME)) {
            LOGGER.error(PROPERTY_IS_MISSED_TEMPLATE, USERNAME_PROPERTY_NAME);
            isMissed = true;
        }

        if (!properties.containsKey(PASSWORD_PROPERTY_NAME)) {
            LOGGER.error(PROPERTY_IS_MISSED_TEMPLATE, PASSWORD_PROPERTY_NAME);
            isMissed = true;
        }

        if (System.getProperty(CATEGORY_PROPERTY_NAME) == null) {
            LOGGER.error("System property '{}' is missed!", CATEGORY_PROPERTY_NAME);
            isMissed = true;
        }

        return isMissed;
    }
}
