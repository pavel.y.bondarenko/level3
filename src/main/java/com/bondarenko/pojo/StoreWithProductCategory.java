package com.bondarenko.pojo;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class StoreWithProductCategory {
    private String cityName;
    private String streetType;
    private String streetName;
    private int houseNumber;
    private String storeName;
    private String categoryName;
    private int amountOfCategory;
}
