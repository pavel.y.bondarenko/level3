package com.bondarenko.pojo;

import jakarta.validation.constraints.Min;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Range;

@Getter
@Setter
@Accessors(chain = true)
public class ProductInStore {

    @Min(value = 1, message = "Store id should be {value} or more")
    private int storeId;

    @Min(value = 1, message = "Product id should be {value} or more")
    private int productId;

    @Min(value = 1, message = "Amount should be {value} or more")
    private int amount;

    @Range(min = 100_000_000, max = 999_999_999, message = "Lot number should be from {min} to {max}")
    private int lotNumber;
}
