package com.bondarenko;

import com.bondarenko.exception.FileAccessException;
import com.bondarenko.exception.PostgresConnectionException;

import java.sql.Connection;

public class DaoFactory {

    public PostgresDao getPostgresDao(Connection connection, FileService fileService) {
        if (connection == null) {
            throw new PostgresConnectionException("Connection should be not null!");
        }

        if (fileService == null) {
            throw new FileAccessException("File service should be not null!");
        }

        return new PostgresDao(connection, fileService);
    }
}
