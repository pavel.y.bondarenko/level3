package com.bondarenko;

import com.bondarenko.pojo.ProductInStore;
import lombok.RequiredArgsConstructor;

import java.util.Random;

@RequiredArgsConstructor
public class ProductInStoreGenerator {
    public static final int MAX_AMOUNT_EXCLUSIVE = 1000;
    public static final int START_LOT_NUMBER = 100_000_000;
    public static final int LAST_LOT_NUMBER_EXCLUSIVE = 1_000_000_000;

    private final int storeMaxId;
    private final int productMaxId;
    private final Random random;

    public ProductInStore generate() {
        int productId = random.ints(1, productMaxId + 1).findFirst().getAsInt();
        int storeId = random.ints(1, storeMaxId + 1).findFirst().getAsInt();
        int amount = random.ints(1, MAX_AMOUNT_EXCLUSIVE).findFirst().getAsInt();
        int lotNumber = random.ints(START_LOT_NUMBER, LAST_LOT_NUMBER_EXCLUSIVE).findFirst().getAsInt();

        return new ProductInStore().setProductId(productId)
                                   .setStoreId(storeId)
                                   .setAmount(amount)
                                   .setLotNumber(lotNumber);
    }
}
