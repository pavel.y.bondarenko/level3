package com.bondarenko;

import com.bondarenko.exception.PropertyFileDoesNotExistsException;
import com.bondarenko.exception.PropertyMissedException;
import org.junit.jupiter.api.*;
import org.mockito.Mockito;

import java.util.concurrent.ExecutorService;

import static org.junit.jupiter.api.Assertions.*;

class PropertyServiceTest {
    private static final String VALID_PROPERTY_FILE_NAME = "valid.properties";
    private static final String URL_MISSED_PROPERTY_FILE_NAME = "url-missed.properties";
    private static final String USERNAME_MISSED_PROPERTY_FILE_NAME = "username-missed.properties";
    private static final String PASSWORD_MISSED_PROPERTY_FILE_NAME = "password-missed.properties";
    private static final String NOT_EXISTS_PROPERTY_FILE_NAME = "not-exists.properties";
    private static final String CATEGORY_PROPERTY_NAME = "category";
    private static final String CATEGORY_VALUE = "some category";
    private static final String URL_VALUE = "url";
    private static final String USERNAME_VALUE = "username";
    private static final String PASSWORD_VALUE = "password";
    private static PropertyService propertyService;

    @BeforeAll
    static void setAll() {
        System.setProperty(CATEGORY_PROPERTY_NAME, CATEGORY_VALUE);
        propertyService = new PropertyService(VALID_PROPERTY_FILE_NAME);
    }

    @AfterAll
    static void clearAll() {
        propertyService = null;
    }

    @BeforeEach
    void setUp() {
        System.setProperty(CATEGORY_PROPERTY_NAME, CATEGORY_VALUE);
    }

    @Test
    void getDbUrlTest() {
        assertEquals(URL_VALUE, propertyService.getDbUrl());
    }

    @Test
    void getDbUsernameTest() {
        assertEquals(USERNAME_VALUE, propertyService.getDbUsername());
    }

    @Test
    void getDbPasswordTest() {
        assertEquals(PASSWORD_VALUE, propertyService.getDbPassword());
    }

    @Test
    void getCategoryTest() {
        assertEquals(CATEGORY_VALUE, propertyService.getCategory());
    }

    @Test
    void missedCategoryPropertyTest() {
        System.clearProperty(CATEGORY_PROPERTY_NAME);
        assertThrows(PropertyMissedException.class, () -> new PropertyService(VALID_PROPERTY_FILE_NAME));
    }

    @Test
    void missedUrlPropertyTest() {
        assertThrows(PropertyMissedException.class, () -> new PropertyService(URL_MISSED_PROPERTY_FILE_NAME));
    }

    @Test
    void missedUsernamePropertyTest() {
        assertThrows(PropertyMissedException.class, () -> new PropertyService(USERNAME_MISSED_PROPERTY_FILE_NAME));
    }

    @Test
    void missedPasswordPropertyTest() {
        assertThrows(PropertyMissedException.class, () -> new PropertyService(PASSWORD_MISSED_PROPERTY_FILE_NAME));
    }

    @Test
    void notExistsPropertyFileTest() {
        assertThrows(PropertyFileDoesNotExistsException.class, () -> new PropertyService(NOT_EXISTS_PROPERTY_FILE_NAME));
    }
}