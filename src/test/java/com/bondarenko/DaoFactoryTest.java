package com.bondarenko;

import com.bondarenko.exception.FileAccessException;
import com.bondarenko.exception.PostgresConnectionException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Connection;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class DaoFactoryTest {

    private DaoFactory daoFactory;

    @Mock
    private Connection connectionMock;

    @Mock
    private FileService fileServiceMock;


    @BeforeEach
    void beforeEach() {
        daoFactory = new DaoFactory();
    }

    @AfterEach
    void afterEach() {
        daoFactory = null;
    }

    @Test
    void getPostgresDaoCorrectTest() {
        daoFactory.getPostgresDao(connectionMock, fileServiceMock);
        verify(fileServiceMock, times(1)).readAllFile(any(String.class));
    }

    @Test
    void getPostgresDaoConnectionIsNullTest() {
        assertThrows(PostgresConnectionException.class, () -> daoFactory.getPostgresDao(null, fileServiceMock));
    }

    @Test
    void getPostgresDaoFileServiceIsNullTest() {
        assertThrows(FileAccessException.class, () -> daoFactory.getPostgresDao(connectionMock, null));
    }
}