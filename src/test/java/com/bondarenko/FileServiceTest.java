package com.bondarenko;

import com.bondarenko.exception.FileAccessException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FileServiceTest {
    private FileService fileService;

    @BeforeEach
    void setUp() {
        fileService = new FileService();
    }

    @AfterEach
    void tearDown() {
        fileService = null;
    }

    @Test
    void readExistsFileTest() {
        String fileName = "exists.file";
        String textInFile = "Text\nfor\ntest";
        String readText = fileService.readAllFile(fileName).replace("\r\n", "\n");

        assertEquals(textInFile, readText);
    }

    @Test
    void readNotExistsFileTest() {
        String fileName = "not-exists.file";

        assertThrows(FileAccessException.class, () -> fileService.readAllFile(fileName));
    }
}