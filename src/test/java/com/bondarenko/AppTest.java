package com.bondarenko;

import com.bondarenko.exception.FileAccessException;
import com.bondarenko.exception.PostgresConnectionException;
import com.bondarenko.pojo.ProductInStore;
import com.bondarenko.pojo.StoreWithProductCategory;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class AppTest {

    private App app;

    @Mock
    private PropertyService propertyServiceMock;

    @Mock
    private FileService fileServiceMock;

    @Mock
    private ExecutorCreator executorCreatorMock;

    @Mock
    private Connection connectionMock;

    @Mock
    private DaoFactory daoFactoryMock;

    @Mock
    private PostgresDao postgresDaoMock;

    @Mock
    private ExecutorService executorServiceWithGeneratorsMock;

    @Mock
    private ExecutorService executorServiceWithBatchSendersMock;

    private BlockingQueue<ProductInStore> queue = new LinkedBlockingQueue<>();

    @BeforeEach
    void setUp() {
        app = new App(propertyServiceMock,
                      fileServiceMock,
                      executorCreatorMock,
                      queue,
                      daoFactoryMock);
    }

    @AfterEach
    void tearDown() {
        app = null;
    }

    @Test
    void runNormalTest() throws InterruptedException {
        String dbUrl = "url";
        String dbUsername = "username";
        String dbPassword = "password";
        String category = "category";
        StoreWithProductCategory storeWithProductCategory = new StoreWithProductCategory().setCategoryName(category)
                                                                                          .setStoreName("store")
                                                                                          .setStreetName("street")
                                                                                          .setCityName("city")
                                                                                          .setAmountOfCategory(999)
                                                                                          .setHouseNumber(777)
                                                                                          .setStreetType("avenue");

        when(propertyServiceMock.getDbUrl()).thenReturn(dbUrl);
        when(propertyServiceMock.getDbUsername()).thenReturn(dbUsername);
        when(propertyServiceMock.getDbPassword()).thenReturn(dbPassword);
        when(propertyServiceMock.getCategory()).thenReturn(category);
        when(postgresDaoMock.getStoreWithMaxNumberOfProductsByCategory(category)).thenReturn(storeWithProductCategory);

        try (MockedStatic<DriverManager> managerMockedStatic = mockStatic(DriverManager.class)) {
            managerMockedStatic.when(() -> DriverManager.getConnection(dbUrl, dbUsername, dbPassword))
                               .thenReturn(connectionMock);
            when(daoFactoryMock.getPostgresDao(connectionMock, fileServiceMock))
                               .thenReturn(postgresDaoMock);
            when(executorCreatorMock.createGeneratorValidatorExecutor(anyInt(),
                                                                      anyInt(),
                                                                      any(),
                                                                      any(ProductInStoreGenerator.class),
                                                                      any(ProductInStoreValidator.class)))
                                    .thenReturn(executorServiceWithGeneratorsMock);
            when(executorCreatorMock.createBatchSenderExecutor(anyInt(),
                                                               any(),
                                                               any(FileService.class),
                                                               anyString(),
                                                               anyString(),
                                                               anyString(),
                                                               anyInt()))
                                    .thenReturn(executorServiceWithBatchSendersMock);

            app.run();

            verify(postgresDaoMock, times(1)).createDatabase();
            verify(postgresDaoMock, times(1)).fillDatabase();
            verify(postgresDaoMock, times(1)).addProductCategories(anyInt(), anyInt());
            verify(postgresDaoMock, times(1)).getCategoryMaxId();
            verify(postgresDaoMock, times(1)).addProducts(anyInt(), anyInt());
            verify(postgresDaoMock, times(1)).getStoreMaxId();
            verify(postgresDaoMock, times(1)).getProductMaxId();
            verify(executorCreatorMock, times(1)).createGeneratorValidatorExecutor(anyInt(),
                                                                                                         anyInt(),
                                                                                                         any(),
                                                                                                         any(ProductInStoreGenerator.class),
                                                                                                         any(ProductInStoreValidator.class));
            verify(executorCreatorMock, times(1)).createBatchSenderExecutor(anyInt(),
                                                                                                  any(),
                                                                                                  any(FileService.class),
                                                                                                  anyString(),
                                                                                                  anyString(),
                                                                                                  anyString(),
                                                                                                  anyInt());
            verify(executorServiceWithGeneratorsMock, times(1)).shutdown();
            verify(executorServiceWithGeneratorsMock, times(1)).awaitTermination(anyLong(), any());
            verify(executorServiceWithBatchSendersMock, times(1)).shutdown();
            verify(executorServiceWithBatchSendersMock, times(1)).awaitTermination(anyLong(), any());
            verify(postgresDaoMock, times(2)).getStoreWithMaxNumberOfProductsByCategory(category);
            verify(postgresDaoMock, times(1)).createIndexes();
        }
    }

    @Test
    void runWithConnectionExceptionTest() {
        String dbUrl = "url";
        String dbUsername = "username";
        String dbPassword = "password";

        when(propertyServiceMock.getDbUrl()).thenReturn(dbUrl);
        when(propertyServiceMock.getDbUsername()).thenReturn(dbUsername);
        when(propertyServiceMock.getDbPassword()).thenReturn(dbPassword);

        try (MockedStatic<DriverManager> managerMockedStatic = mockStatic(DriverManager.class)) {
            managerMockedStatic.when(() -> DriverManager.getConnection(dbUrl, dbUsername, dbPassword))
                    .thenThrow(SQLException.class);
            assertDoesNotThrow(() -> app.run());
        }
    }

    @Test
    void runWithFileAccessExceptionTest() {
        String dbUrl = "url";
        String dbUsername = "username";
        String dbPassword = "password";

        when(propertyServiceMock.getDbUrl()).thenReturn(dbUrl);
        when(propertyServiceMock.getDbUsername()).thenReturn(dbUsername);
        when(propertyServiceMock.getDbPassword()).thenReturn(dbPassword);

        try (MockedStatic<DriverManager> managerMockedStatic = mockStatic(DriverManager.class)) {
            managerMockedStatic.when(() -> DriverManager.getConnection(dbUrl, dbUsername, dbPassword))
                               .thenReturn(connectionMock);
            when(daoFactoryMock.getPostgresDao(connectionMock, fileServiceMock))
                               .thenThrow(FileAccessException.class);
            assertDoesNotThrow(() -> app.run());
        }
    }

    @Test
    void runWithPostgresConnectionExceptionTest() {
        String dbUrl = "url";
        String dbUsername = "username";
        String dbPassword = "password";

        when(propertyServiceMock.getDbUrl()).thenReturn(dbUrl);
        when(propertyServiceMock.getDbUsername()).thenReturn(dbUsername);
        when(propertyServiceMock.getDbPassword()).thenReturn(dbPassword);

        try (MockedStatic<DriverManager> managerMockedStatic = mockStatic(DriverManager.class)) {
            managerMockedStatic.when(() -> DriverManager.getConnection(dbUrl, dbUsername, dbPassword))
                    .thenReturn(connectionMock);
            when(daoFactoryMock.getPostgresDao(connectionMock, fileServiceMock))
                    .thenThrow(PostgresConnectionException.class);
            assertDoesNotThrow(() -> app.run());
        }
    }

    @Test
    void runWithInterruptedExceptionTest() throws InterruptedException {
        String dbUrl = "url";
        String dbUsername = "username";
        String dbPassword = "password";
        String category = "category";
        StoreWithProductCategory storeWithProductCategory = new StoreWithProductCategory().setCategoryName(category)
                .setStoreName("store")
                .setStreetName("street")
                .setCityName("city")
                .setAmountOfCategory(999)
                .setHouseNumber(777)
                .setStreetType("avenue");

        when(propertyServiceMock.getDbUrl()).thenReturn(dbUrl);
        when(propertyServiceMock.getDbUsername()).thenReturn(dbUsername);
        when(propertyServiceMock.getDbPassword()).thenReturn(dbPassword);
        when(propertyServiceMock.getCategory()).thenReturn(category);
        when(postgresDaoMock.getStoreWithMaxNumberOfProductsByCategory(category)).thenReturn(storeWithProductCategory);

        try (MockedStatic<DriverManager> managerMockedStatic = mockStatic(DriverManager.class)) {
            managerMockedStatic.when(() -> DriverManager.getConnection(dbUrl, dbUsername, dbPassword))
                    .thenReturn(connectionMock);
            when(daoFactoryMock.getPostgresDao(connectionMock, fileServiceMock))
                    .thenReturn(postgresDaoMock);
            when(executorCreatorMock.createGeneratorValidatorExecutor(anyInt(),
                    anyInt(),
                    any(),
                    any(ProductInStoreGenerator.class),
                    any(ProductInStoreValidator.class)))
                    .thenReturn(executorServiceWithGeneratorsMock);
            when(executorCreatorMock.createBatchSenderExecutor(anyInt(),
                    any(),
                    any(FileService.class),
                    anyString(),
                    anyString(),
                    anyString(),
                    anyInt()))
                    .thenReturn(executorServiceWithBatchSendersMock);

            when(executorServiceWithGeneratorsMock.awaitTermination(anyLong(), any())).thenThrow(InterruptedException.class);
            when(executorServiceWithBatchSendersMock.awaitTermination(anyLong(), any())).thenReturn(false);

            app.run();

            verify(executorServiceWithGeneratorsMock, times(1)).shutdownNow();
            verify(executorServiceWithBatchSendersMock, times(1)).shutdownNow();
        }
    }
}