package com.bondarenko;

import com.bondarenko.pojo.ProductInStore;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

class ProductInStoreValidatorTest {
    private static ProductInStoreValidator productInStoreValidator;
    private ProductInStore productInStore;

    @BeforeAll
    static void setAll() {
        productInStoreValidator = new ProductInStoreValidator();
    }

    @AfterAll
    static void clearAll() {
        productInStoreValidator = null;
    }

    @BeforeEach
    void setUp() {
        productInStore = new ProductInStore().setProductId(1)
                                             .setStoreId(1)
                                             .setAmount(1)
                                             .setLotNumber(999_999_999);
    }

    @AfterEach
    void tearDown() {
        productInStore = null;
    }

    @Test
    void isValidCorrectTest() {
        assertTrue(productInStoreValidator.isValid(productInStore));
    }

    @Test
    void isValidIncorrectStoreIdTest() {
        int incorrectStoreId = 0;
        assertFalse(productInStoreValidator.isValid(productInStore.setStoreId(incorrectStoreId)));
    }

    @Test
    void isValidIncorrectProductIdTest() {
        int incorrectProductId = 0;
        assertFalse(productInStoreValidator.isValid(productInStore.setProductId(incorrectProductId)));
    }

    @Test
    void isValidIncorrectAmountTest() {
        int incorrectAmount = 0;
        assertFalse(productInStoreValidator.isValid(productInStore.setAmount(incorrectAmount)));
    }

    @Test
    void isValidIncorrectLotNumberTest() {
        int incorrectLowerLotNumber = 99_999_999;
        assertFalse(productInStoreValidator.isValid(productInStore.setLotNumber(incorrectLowerLotNumber)));

        int incorrectHigherLotNumber = 1_000_000_000;
        assertFalse(productInStoreValidator.isValid(productInStore.setLotNumber(incorrectHigherLotNumber)));
    }
}