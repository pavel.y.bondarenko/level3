package com.bondarenko;

import com.bondarenko.exception.PostgresConnectionException;
import com.bondarenko.pojo.StoreWithProductCategory;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class PostgresDaoTest {

    private PostgresDao postgresDao;
    private String sqlForFind = "sql for find";

    @Mock
    private Connection connectionMock;

    @Mock
    private FileService fileServiceMock;

    @Mock
    private Statement statementMock;

    @Mock
    private PreparedStatement preparedStatementMock;

    @Mock
    private ResultSet resultSetMock;

    @BeforeEach
    void setEach() {
        String findProductInStoreWithMaxAmountByCategorySqlFileName = "find_store_with_max_products_by_category.sql";
        when(fileServiceMock.readAllFile(findProductInStoreWithMaxAmountByCategorySqlFileName)).thenReturn(sqlForFind);
        postgresDao = new PostgresDao(connectionMock, fileServiceMock);
    }

    @AfterEach
    void afterEach() {
        postgresDao = null;
    }

    @Test
    void getStoreMaxIdTest() throws SQLException {
        String storeMaxIdSql = "select max(id) from store";
        int maxStoreId = 15;

        when(connectionMock.createStatement()).thenReturn(statementMock);
        when(statementMock.executeQuery(storeMaxIdSql)).thenReturn(resultSetMock);
        when(resultSetMock.next()).thenReturn(true);
        when(resultSetMock.getInt(1)).thenReturn(maxStoreId);

        assertEquals(maxStoreId, postgresDao.getStoreMaxId());
    }

    @Test
    void getStoreMaxIdWithoutRawsTest() throws SQLException {
        String storeMaxIdSql = "select max(id) from store";
        int maxStoreId = 0;

        when(connectionMock.createStatement()).thenReturn(statementMock);
        when(statementMock.executeQuery(storeMaxIdSql)).thenReturn(resultSetMock);
        when(resultSetMock.next()).thenReturn(true);
        when(resultSetMock.getInt(1)).thenReturn(maxStoreId);

        assertThrows(PostgresConnectionException.class, () -> postgresDao.getStoreMaxId());
    }

    @Test
    void getStoreMaxIdResultNextIsFalseTest() throws SQLException {
        String storeMaxIdSql = "select max(id) from store";

        when(connectionMock.createStatement()).thenReturn(statementMock);
        when(statementMock.executeQuery(storeMaxIdSql)).thenReturn(resultSetMock);
        when(resultSetMock.next()).thenReturn(false);

        assertThrows(PostgresConnectionException.class, () -> postgresDao.getStoreMaxId());
    }

    @Test
    void getStoreMaxIdExecuteQueryExceptionTest() throws SQLException {
        String storeMaxIdSql = "select max(id) from store";

        when(connectionMock.createStatement()).thenReturn(statementMock);
        when(statementMock.executeQuery(storeMaxIdSql)).thenThrow(SQLException.class);

        assertThrows(PostgresConnectionException.class, () -> postgresDao.getStoreMaxId());
    }

    @Test
    void getProductMaxIdTest() throws SQLException {
        String productMaxIdSql = "select max(id) from product";
        int maxProductId = 15;

        when(connectionMock.createStatement()).thenReturn(statementMock);
        when(statementMock.executeQuery(productMaxIdSql)).thenReturn(resultSetMock);
        when(resultSetMock.next()).thenReturn(true);
        when(resultSetMock.getInt(1)).thenReturn(maxProductId);

        assertEquals(maxProductId, postgresDao.getProductMaxId());
    }

    @Test
    void getProductMaxIdWithoutRawsTest() throws SQLException {
        String productMaxIdSql = "select max(id) from product";
        int maxProductId = 0;

        when(connectionMock.createStatement()).thenReturn(statementMock);
        when(statementMock.executeQuery(productMaxIdSql)).thenReturn(resultSetMock);
        when(resultSetMock.next()).thenReturn(true);
        when(resultSetMock.getInt(1)).thenReturn(maxProductId);

        assertThrows(PostgresConnectionException.class, () -> postgresDao.getProductMaxId());
    }

    @Test
    void getProductMaxIdResultNextIsFalseTest() throws SQLException {
        String productMaxIdSql = "select max(id) from product";

        when(connectionMock.createStatement()).thenReturn(statementMock);
        when(statementMock.executeQuery(productMaxIdSql)).thenReturn(resultSetMock);
        when(resultSetMock.next()).thenReturn(false);

        assertThrows(PostgresConnectionException.class, () -> postgresDao.getProductMaxId());
    }

    @Test
    void getProductMaxIdExecuteQueryExceptionTest() throws SQLException {
        String productMaxIdSql = "select max(id) from product";

        when(connectionMock.createStatement()).thenReturn(statementMock);
        when(statementMock.executeQuery(productMaxIdSql)).thenThrow(SQLException.class);

        assertThrows(PostgresConnectionException.class, () -> postgresDao.getProductMaxId());
    }

    @Test
    void getCategoryMaxIdTest() throws SQLException {
        String categoryMaxIdSql = "select max(id) from product_category";
        int maxCategoryId = 15;

        when(connectionMock.createStatement()).thenReturn(statementMock);
        when(statementMock.executeQuery(categoryMaxIdSql)).thenReturn(resultSetMock);
        when(resultSetMock.next()).thenReturn(true);
        when(resultSetMock.getInt(1)).thenReturn(maxCategoryId);

        assertEquals(maxCategoryId, postgresDao.getCategoryMaxId());
    }

    @Test
    void getCategoryMaxIdWithoutRawsTest() throws SQLException {
        String categoryMaxIdSql = "select max(id) from product_category";
        int maxCategoryId = 0;

        when(connectionMock.createStatement()).thenReturn(statementMock);
        when(statementMock.executeQuery(categoryMaxIdSql)).thenReturn(resultSetMock);
        when(resultSetMock.next()).thenReturn(true);
        when(resultSetMock.getInt(1)).thenReturn(maxCategoryId);

        assertThrows(PostgresConnectionException.class, () -> postgresDao.getCategoryMaxId());
    }

    @Test
    void getCategoryMaxIdResultNextIsFalseTest() throws SQLException {
        String categoryMaxIdSql = "select max(id) from product_category";

        when(connectionMock.createStatement()).thenReturn(statementMock);
        when(statementMock.executeQuery(categoryMaxIdSql)).thenReturn(resultSetMock);
        when(resultSetMock.next()).thenReturn(false);

        assertThrows(PostgresConnectionException.class, () -> postgresDao.getCategoryMaxId());
    }

    @Test
    void getCategoryMaxIdExecuteQueryExceptionTest() throws SQLException {
        String categoryMaxIdSql = "select max(id) from product_category";

        when(connectionMock.createStatement()).thenReturn(statementMock);
        when(statementMock.executeQuery(categoryMaxIdSql)).thenThrow(SQLException.class);

        assertThrows(PostgresConnectionException.class, () -> postgresDao.getCategoryMaxId());
    }

    @Test
    void addProductCategoriesTest() throws SQLException {
        String addSqlTemplate = "insert into product_category (name) values (?)";
        int numberOfCategories = 1_000;
        int batchMaxSize = 100;
        int numberOfBatches = 10;

        when(connectionMock.prepareStatement(addSqlTemplate)).thenReturn(preparedStatementMock);
        postgresDao.addProductCategories(numberOfCategories, batchMaxSize);

        verify(preparedStatementMock, times(numberOfCategories)).setString(anyInt(), any(String.class));
        verify(preparedStatementMock, times(numberOfCategories)).addBatch();
        verify(preparedStatementMock, times(numberOfBatches)).executeBatch();
    }

    @Test
    void addProductCategoriesCanNotCreateStatementTest() throws SQLException {
        String addSqlTemplate = "insert into product_category (name) values (?)";
        int numberOfCategories = 1_000;
        int batchMaxSize = 100;

        when(connectionMock.prepareStatement(addSqlTemplate)).thenThrow(SQLException.class);
        assertThrows(PostgresConnectionException.class, () -> postgresDao.addProductCategories(numberOfCategories, batchMaxSize));
    }

    @Test
    void addProductCategoriesBatchExecuteExceptionTest() throws SQLException {
        String addSqlTemplate = "insert into product_category (name) values (?)";
        int numberOfCategories = 1_000;
        int batchMaxSize = 100;

        when(connectionMock.prepareStatement(addSqlTemplate)).thenReturn(preparedStatementMock);
        when(preparedStatementMock.executeBatch()).thenThrow(SQLException.class);
        assertThrows(PostgresConnectionException.class, () -> postgresDao.addProductCategories(numberOfCategories, batchMaxSize));
    }

    @Test
    void addProductsTest() throws SQLException {
        String addSqlTemplate = "insert into product (name, category_id) values (?, ?)";
        int categoryMaxId = 100;
        int numberOfProductsPerCategory = 1_000;
        int totalNumberOfProducts = categoryMaxId * numberOfProductsPerCategory;

        when(connectionMock.prepareStatement(addSqlTemplate)).thenReturn(preparedStatementMock);
        postgresDao.addProducts(categoryMaxId, numberOfProductsPerCategory);

        verify(preparedStatementMock, times(totalNumberOfProducts)).setString(anyInt(), any(String.class));
        verify(preparedStatementMock, times(totalNumberOfProducts)).setInt(anyInt(), anyInt());
        verify(preparedStatementMock, times(totalNumberOfProducts)).addBatch();
        verify(preparedStatementMock, times(categoryMaxId)).executeBatch();
    }

    @Test
    void addProductsCanNotCreateStatementTest() throws SQLException {
        String addSqlTemplate = "insert into product (name, category_id) values (?, ?)";
        int categoryMaxId = 100;
        int numberOfProductsPerCategory = 1_000;

        when(connectionMock.prepareStatement(addSqlTemplate)).thenThrow(SQLException.class);
        assertThrows(PostgresConnectionException.class, () -> postgresDao.addProducts(categoryMaxId, numberOfProductsPerCategory));
    }

    @Test
    void addProductsBatchExecuteExceptionTest() throws SQLException {
        String addSqlTemplate = "insert into product (name, category_id) values (?, ?)";
        int categoryMaxId = 100;
        int numberOfProductsPerCategory = 1_000;

        when(connectionMock.prepareStatement(addSqlTemplate)).thenReturn(preparedStatementMock);
        when(preparedStatementMock.executeBatch()).thenThrow(SQLException.class);
        assertThrows(PostgresConnectionException.class, () -> postgresDao.addProducts(categoryMaxId, numberOfProductsPerCategory));
    }

    @Test
    void addProductsAddBatchExceptionTest() throws SQLException {
        String addSqlTemplate = "insert into product (name, category_id) values (?, ?)";
        int categoryMaxId = 100;
        int numberOfProductsPerCategory = 1_000;

        when(connectionMock.prepareStatement(addSqlTemplate)).thenReturn(preparedStatementMock);
        doThrow(SQLException.class).when(preparedStatementMock).addBatch();
        assertThrows(PostgresConnectionException.class, () -> postgresDao.addProducts(categoryMaxId, numberOfProductsPerCategory));
    }

    @Test
    void getPreparedStatementForAddProductInStoreTest() throws SQLException {
        String addProductInStoreSqlTemplate = "insert into product_in_store "
                                            + "(store_id, product_id, amount, lot_number) "
                                            + "values (?, ?, ?, ?)";
        when(connectionMock.prepareStatement(addProductInStoreSqlTemplate)).thenReturn(preparedStatementMock);
        assertEquals(preparedStatementMock, postgresDao.getPreparedStatementForAddProductInStore());
    }

    @Test
    void getPreparedStatementForAddProductInStoreConnectionFailedTest() throws SQLException {
        String addProductInStoreSqlTemplate = "insert into product_in_store "
                                            + "(store_id, product_id, amount, lot_number) "
                                            + "values (?, ?, ?, ?)";
        when(connectionMock.prepareStatement(addProductInStoreSqlTemplate)).thenThrow(SQLException.class);
        assertThrows(PostgresConnectionException.class, () -> postgresDao.getPreparedStatementForAddProductInStore());
    }

    @Test
    void createDatabaseTest() throws SQLException {
        String ddlSqlFilename = "start_database.ddl";
        String sql = "";

        when(fileServiceMock.readAllFile(ddlSqlFilename)).thenReturn(sql);
        when(connectionMock.createStatement()).thenReturn(statementMock);

        postgresDao.createDatabase();

        verify(statementMock, times(1)).execute(sql);
    }

    @Test
    void createDatabaseExecuteExceptionTest() throws SQLException {
        String ddlSqlFilename = "start_database.ddl";
        String sql = "";

        when(fileServiceMock.readAllFile(ddlSqlFilename)).thenReturn(sql);
        when(connectionMock.createStatement()).thenReturn(statementMock);
        when(statementMock.execute(sql)).thenThrow(SQLException.class);

        assertThrows(PostgresConnectionException.class, () -> postgresDao.createDatabase());

        verify(statementMock, times(1)).execute(sql);
    }

    @Test
    void fillDatabaseTest() throws SQLException {
        String dataSqlFilename = "data.sql";
        String sql = "";

        when(fileServiceMock.readAllFile(dataSqlFilename)).thenReturn(sql);
        when(connectionMock.createStatement()).thenReturn(statementMock);

        postgresDao.fillDatabase();

        verify(statementMock, times(1)).execute(sql);
    }

    @Test
    void fillDatabaseExecuteExceptionTest() throws SQLException {
        String dataSqlFilename = "data.sql";
        String sql = "";

        when(fileServiceMock.readAllFile(dataSqlFilename)).thenReturn(sql);
        when(connectionMock.createStatement()).thenReturn(statementMock);
        when(statementMock.execute(sql)).thenThrow(SQLException.class);

        assertThrows(PostgresConnectionException.class, () -> postgresDao.fillDatabase());

        verify(statementMock, times(1)).execute(sql);
    }

    @Test
    void createIndexesTest() throws SQLException {
        String indexesSqlFilename = "indexes.sql";
        String sql = "";

        when(fileServiceMock.readAllFile(indexesSqlFilename)).thenReturn(sql);
        when(connectionMock.createStatement()).thenReturn(statementMock);

        postgresDao.createIndexes();

        verify(statementMock, times(1)).execute(sql);
    }

    @Test
    void createIndexesExecuteExceptionTest() throws SQLException {
        String indexesSqlFilename = "indexes.sql";
        String sql = "";

        when(fileServiceMock.readAllFile(indexesSqlFilename)).thenReturn(sql);
        when(connectionMock.createStatement()).thenReturn(statementMock);
        when(statementMock.execute(sql)).thenThrow(SQLException.class);

        assertThrows(PostgresConnectionException.class, () -> postgresDao.createIndexes());

        verify(statementMock, times(1)).execute(sql);
    }

    @Test
    void getStoreWithMaxNumberOfProductsByCategoryTest() throws SQLException {
        String cityColumnName = "city";
        String streetTypeColumnName = "s_type";
        String streetColumnName = "street";
        String numberColumnName = "number";
        String storeColumnName = "store_name";
        String amountColumnName = "amount";

        String category = "category";
        String city = "London";
        String streetType = "avenue";
        String street = "Some";
        int number = 101;
        String storeName = "Toy land";
        int amount = 1_000;

        when(connectionMock.prepareStatement(sqlForFind)).thenReturn(preparedStatementMock);
        when(preparedStatementMock.executeQuery()).thenReturn(resultSetMock);
        when(resultSetMock.next()).thenReturn(true);
        when(resultSetMock.getString(cityColumnName)).thenReturn(city);
        when(resultSetMock.getString(streetTypeColumnName)).thenReturn(streetType);
        when(resultSetMock.getString(streetColumnName)).thenReturn(street);
        when(resultSetMock.getInt(numberColumnName)).thenReturn(number);
        when(resultSetMock.getString(storeColumnName)).thenReturn(storeName);
        when(resultSetMock.getInt(amountColumnName)).thenReturn(amount);

        StoreWithProductCategory store = postgresDao.getStoreWithMaxNumberOfProductsByCategory(category);
        assertNotNull(store);
        assertEquals(category, store.getCategoryName());
        assertEquals(city, store.getCityName());
        assertEquals(streetType, store.getStreetType());
        assertEquals(street, store.getStreetName());
        assertEquals(number, store.getHouseNumber());
        assertEquals(storeName, store.getStoreName());
        assertEquals(amount, store.getAmountOfCategory());

        verify(preparedStatementMock, times(1)).setString(1, category);
        verify(preparedStatementMock, times(1)).executeQuery();
        verify(resultSetMock, times(1)).next();
        verify(resultSetMock, times(1)).getString(cityColumnName);
        verify(resultSetMock, times(1)).getString(streetTypeColumnName);
        verify(resultSetMock, times(1)).getString(streetColumnName);
        verify(resultSetMock, times(1)).getInt(numberColumnName);
        verify(resultSetMock, times(1)).getString(storeColumnName);
        verify(resultSetMock, times(1)).getInt(amountColumnName);
    }

    @Test
    void getStoreWithMaxNumberOfProductsByCategoryConnectionExceptionTest() throws SQLException {
        when(connectionMock.prepareStatement(sqlForFind)).thenThrow(SQLException.class);
        assertThrows(PostgresConnectionException.class, () -> postgresDao.getStoreWithMaxNumberOfProductsByCategory("some category"));
    }
}