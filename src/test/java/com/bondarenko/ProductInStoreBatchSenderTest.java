package com.bondarenko;

import com.bondarenko.exception.PostgresConnectionException;
import com.bondarenko.pojo.ProductInStore;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ProductInStoreBatchSenderTest {
    private static final String DB_URL = "url";
    private static final String DB_USERNAME = "username";
    private static final String DB_PASSWORD = "password";
    private ProductInStoreBatchSender productInStoreBatchSender;

    private final BlockingQueue<ProductInStore> productInStoreQueue = new LinkedBlockingQueue<>();

    @Mock
    private FileService fileServiceMock;

    @Mock
    private PreparedStatement preparedStatementMock;

    @Mock
    private Connection connectionMock;

    @BeforeEach
    void setUp() {
        int batchMaxSize = 2;
        productInStoreBatchSender = new ProductInStoreBatchSender(productInStoreQueue,
                                                                  fileServiceMock,
                                                                  DB_URL,
                                                                  DB_USERNAME,
                                                                  DB_PASSWORD,
                                                                  batchMaxSize);


        ProductInStore productInStore = new ProductInStore().setStoreId(1)
                                                            .setProductId(2)
                                                            .setAmount(3)
                                                            .setLotNumber(4);
        ProductInStore poisonProductInStore = new ProductInStore();

        productInStoreQueue.add(productInStore);
        productInStoreQueue.add(productInStore);
        productInStoreQueue.add(productInStore);
        productInStoreQueue.add(productInStore);
        productInStoreQueue.add(productInStore);
        productInStoreQueue.add(poisonProductInStore);
    }

    @AfterEach
    void tearDown() {
        productInStoreBatchSender = null;
    }

    @Test
    void runTest() throws SQLException {
        int numberOfObjectsInQueue = 5;
        int numberOfBatches = 3;
        when(connectionMock.prepareStatement(anyString())).thenReturn(preparedStatementMock);
        when(preparedStatementMock.executeBatch()).thenReturn(null);

        try (MockedStatic<DriverManager> managerMockedStatic = mockStatic(DriverManager.class)) {
            managerMockedStatic.when(() -> DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD))
                               .thenReturn(connectionMock);
            productInStoreBatchSender.run();
        }

        verify(preparedStatementMock, times(numberOfObjectsInQueue)).setInt(1, 1);
        verify(preparedStatementMock, times(numberOfObjectsInQueue)).setInt(2, 2);
        verify(preparedStatementMock, times(numberOfObjectsInQueue)).setInt(3, 3);
        verify(preparedStatementMock, times(numberOfObjectsInQueue)).setInt(4, 4);
        verify(preparedStatementMock, times(numberOfObjectsInQueue)).addBatch();
        verify(preparedStatementMock, times(numberOfBatches)).executeBatch();
    }

    @Test
    void runWithExceptionTest() throws SQLException {
        when(connectionMock.prepareStatement(anyString())).thenReturn(preparedStatementMock);
        when(preparedStatementMock.executeBatch()).thenThrow(SQLException.class);
        try (MockedStatic<DriverManager> managerMockedStatic = mockStatic(DriverManager.class)) {
            managerMockedStatic.when(() -> DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD))
                    .thenReturn(connectionMock);
            assertThrows(PostgresConnectionException.class, () -> productInStoreBatchSender.run());
        }
    }

    @Test
    void runWithExceptionInConnectionTest() {
        try (MockedStatic<DriverManager> managerMockedStatic = mockStatic(DriverManager.class)) {
            managerMockedStatic.when(() -> DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD))
                    .thenThrow(SQLException.class);
            assertThrows(PostgresConnectionException.class, () -> productInStoreBatchSender.run());
        }
    }
}