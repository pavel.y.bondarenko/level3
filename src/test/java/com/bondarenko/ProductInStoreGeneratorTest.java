package com.bondarenko;

import com.bondarenko.pojo.ProductInStore;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Random;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class ProductInStoreGeneratorTest {
    private static final int STORE_MAX_ID = 10;
    private static final int PRODUCT_MAX_ID = 15;
    private ProductInStoreGenerator productInStoreGenerator;

    @Mock
    private Random randomMock;

    @BeforeEach
    void setUp() {
        productInStoreGenerator = new ProductInStoreGenerator(STORE_MAX_ID, PRODUCT_MAX_ID, randomMock);
    }

    @AfterEach
    void tearDown() {
        productInStoreGenerator = null;
    }

    @Test
    void generateTest() {
        int storeId = 5;
        int productId = 10;
        int amount = 100;
        int lotNumber = 111_111_111;

        Mockito.when(randomMock.ints(1, STORE_MAX_ID + 1))
               .thenReturn(IntStream.of(storeId));
        Mockito.when(randomMock.ints(1, PRODUCT_MAX_ID + 1))
               .thenReturn(IntStream.of(productId));
        Mockito.when(randomMock.ints(1, ProductInStoreGenerator.MAX_AMOUNT_EXCLUSIVE))
               .thenReturn(IntStream.of(amount));
        Mockito.when(randomMock.ints(ProductInStoreGenerator.START_LOT_NUMBER, ProductInStoreGenerator.LAST_LOT_NUMBER_EXCLUSIVE))
               .thenReturn(IntStream.of(lotNumber));

        ProductInStore productInStore = productInStoreGenerator.generate();
        assertEquals(storeId, productInStore.getStoreId());
        assertEquals(productId, productInStore.getProductId());
        assertEquals(amount, productInStore.getAmount());
        assertEquals(lotNumber, productInStore.getLotNumber());
    }
}